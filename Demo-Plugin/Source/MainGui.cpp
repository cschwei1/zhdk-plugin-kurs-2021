/*
  ==============================================================================

  This is an automatically generated GUI class created by the Projucer!

  Be careful when adding custom code to these files, as only the code within
  the "//[xyz]" and "//[/xyz]" sections will be retained when the file is loaded
  and re-saved.

  Created with Projucer version: 6.0.7

  ------------------------------------------------------------------------------

  The Projucer is part of the JUCE library.
  Copyright (c) 2020 - Raw Material Software Limited.

  ==============================================================================
*/

//[Headers] You can add your own extra header files here...
//[/Headers]

#include "MainGui.h"


//[MiscUserDefs] You can add your own user definitions and misc code here...
//[/MiscUserDefs]

//==============================================================================
MainGui::MainGui (AudioParams* pAudioParams)
{
    //[Constructor_pre] You can add your own custom stuff here..
    
    // we keep a reference to our audio parameters
    pParams = pAudioParams;
    
    // we tell the audio parameters that we (this = the instance of MainGui we are creating using this constructor) are interested in parameter changes
    pParams->a->addListener(this);
    pParams->e->addListener(this);
    pParams->d->addListener(this);
    //[/Constructor_pre]

    buttonAction.reset (new juce::TextButton ("buttonAction"));
    addAndMakeVisible (buttonAction.get());
    buttonAction->setButtonText (TRANS("click me!"));
    buttonAction->addListener (this);

    positionDisplay.reset (new PositionDisplay (pParams));
    addAndMakeVisible (positionDisplay.get());
    positionDisplay->setName ("new component");


    //[UserPreSize]
    //[/UserPreSize]

    setSize (600, 400);


    //[Constructor] You can add your own custom stuff here..
    //[/Constructor]
}

MainGui::~MainGui()
{
    //[Destructor_pre]. You can add your own custom destruction code here..
    // IMPORTANT: we have to tell the parameters that we are no longer interested because we (this instance of MainGui) is being destroyed!
    pParams->a->removeListener(this);
    pParams->e->removeListener(this);
    pParams->d->removeListener(this);
    //[/Destructor_pre]

    buttonAction = nullptr;
    positionDisplay = nullptr;


    //[Destructor]. You can add your own custom destruction code here..
    //[/Destructor]
}

//==============================================================================
void MainGui::paint (juce::Graphics& g)
{
    //[UserPrePaint] Add your own custom painting code here..
    //[/UserPrePaint]

    g.fillAll (juce::Colour (0xff323e44));

    //[UserPaint] Add your own custom painting code here..
    //[/UserPaint]
}

void MainGui::resized()
{
    //[UserPreResize] Add your own custom resize code here..
    //[/UserPreResize]

    buttonAction->setBounds (8, 8, getWidth() - 16, 24);
    positionDisplay->setBounds (8, 48, getWidth() - 16, getHeight() - 54);
    //[UserResized] Add your own custom resize handling here..
    //[/UserResized]
}

void MainGui::buttonClicked (juce::Button* buttonThatWasClicked)
{
    //[UserbuttonClicked_Pre]
    //[/UserbuttonClicked_Pre]

    if (buttonThatWasClicked == buttonAction.get())
    {
        //[UserButtonCode_buttonAction] -- add your button handler code here..
        
        // produce some message box with more or less meaningful content
        // AlertWindow::showMessageBoxAsync(AlertWindow::AlertIconType::InfoIcon, "Test " + String(pParams->a->get()), "You clicked me!");
        
        // manually update the position display
        //updatePosition();

        // or set our audio parameters to random (valid) values
        Random rnd;
        *pParams->a = rnd.nextInt(360);
        *pParams->e = rnd.nextInt(90) - 90;
        *pParams->d = rnd.nextInt(10) / 10.0f;
        //[/UserButtonCode_buttonAction]
    }

    //[UserbuttonClicked_Post]
    //[/UserbuttonClicked_Post]
}



//[MiscUserCode] You can add your own definitions of your custom methods or any other code here...
void MainGui::updatePosition()
{
    // tell the position display to repaint the component (internally call the paint function)
    positionDisplay->repaint();
}
//[/MiscUserCode]


//==============================================================================
#if 0
/*  -- Projucer information section --

    This is where the Projucer stores the metadata that describe this GUI layout, so
    make changes in here at your peril!

BEGIN_JUCER_METADATA

<JUCER_COMPONENT documentType="Component" className="MainGui" componentName=""
                 parentClasses="public juce::Component, public AudioProcessorParameter::Listener, public AsyncUpdater"
                 constructorParams="AudioParams* pAudioParams" variableInitialisers=""
                 snapPixels="8" snapActive="1" snapShown="1" overlayOpacity="0.330"
                 fixedSize="0" initialWidth="600" initialHeight="400">
  <BACKGROUND backgroundColour="ff323e44"/>
  <TEXTBUTTON name="buttonAction" id="48346f05801f4060" memberName="buttonAction"
              virtualName="" explicitFocusOrder="0" pos="8 8 16M 24" buttonText="click me!"
              connectedEdges="0" needsCallback="1" radioGroupId="0"/>
  <GENERICCOMPONENT name="new component" id="43350a6a150cd985" memberName="positionDisplay"
                    virtualName="" explicitFocusOrder="0" pos="8 48 16M 54M" class="PositionDisplay"
                    params="pParams"/>
</JUCER_COMPONENT>

END_JUCER_METADATA
*/
#endif


//[EndFile] You can add extra defines here...
//[/EndFile]

