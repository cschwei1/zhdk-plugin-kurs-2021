/*
  ==============================================================================

    AudioParams.h
    Created: 31 Mar 2021 2:12:29pm
    Author:  Schweizer Christian

  ==============================================================================
*/

#pragma once
class AudioParams
{
    // this class holds references to the actual audio parameters
    
public:
    AudioParameterFloat* a;
    AudioParameterFloat* e;
    AudioParameterFloat* d;
};
