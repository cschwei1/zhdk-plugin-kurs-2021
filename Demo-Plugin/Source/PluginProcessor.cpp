/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
DemoPluginAudioProcessor::DemoPluginAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
    // initialize class variables to avoid undefined values and big surprises!
    runningValue = 0;
    
    // create the actual instances of the audio parameters and save the pointer to our audio params object
    audioParams.a = new AudioParameterFloat("Azimuth", "Azimuth", NormalisableRange<float>(0.0f, 360.0f), 0.0f);
    audioParams.e = new AudioParameterFloat("Elevation", "Elevation", NormalisableRange<float>(-90.0f, 90.0f), 0.0f);
    audioParams.d = new AudioParameterFloat("Distance", "Distance", NormalisableRange<float>(0.0f, 1.0f), 0.5f);
    
    // tell JUCE to add the parameter to the plugin and maintain all future actions with these parameters (e.g. delete if not used anymore)
    addParameter(audioParams.a);
    addParameter(audioParams.e);
    addParameter(audioParams.d);
}

DemoPluginAudioProcessor::~DemoPluginAudioProcessor()
{
}

//==============================================================================
const juce::String DemoPluginAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool DemoPluginAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool DemoPluginAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool DemoPluginAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double DemoPluginAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int DemoPluginAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int DemoPluginAudioProcessor::getCurrentProgram()
{
    return 0;
}

void DemoPluginAudioProcessor::setCurrentProgram (int index)
{
}

const juce::String DemoPluginAudioProcessor::getProgramName (int index)
{
    return {};
}

void DemoPluginAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void DemoPluginAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
}

void DemoPluginAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool DemoPluginAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void DemoPluginAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    // Make sure to reset the state if your inner loop is processing
    // the samples and the outer loop is handling the channels.
    // Alternatively, you can process the samples with the channels
    // interleaved by keeping the same state.
    
    // first we copy our audio input
    AudioBuffer<float> input(1, buffer.getNumSamples());
    input.copyFrom(0, 0, buffer, 0, 0, buffer.getNumSamples());
    
    // we ask for a pointer to our readable data
    const float* readPointer = input.getReadPointer(0);
    
    // get snapshot of current AED coordinates and convert angles from degrees to radians
    float pi = MathConstants<float>::pi;
    float a = -audioParams.a->get() * pi / 180.0f; // our azimuth is in Navigation direction (clockwise), in mathematics we need conter-clock-wise!
    float e = audioParams.e->get() * pi / 180.0f;
    float d = audioParams.d->get();
    
    // calculate distance attenuations
    float unitCircle = 0.1f;
    float scaledDistance = jmax(unitCircle, d * (1.0f / unitCircle));
    float distanceFactorW = atan(scaledDistance * pi / 2.0f) / (scaledDistance * pi / 2.0f);
    float distanceFactorOthers = (1 - exp(-scaledDistance)) * distanceFactorW;
    
    // calculate ambisonics (B-format) factors
    float ambiFactors[4];
    ambiFactors[0] = distanceFactorW * 1.0f;
    ambiFactors[1] = distanceFactorOthers * cos(e) * sin(a);
    ambiFactors[2] = distanceFactorOthers * sin(e);
    ambiFactors[3] = distanceFactorOthers * cos(e) * cos(a);
    
    // we loop through all output channels
    for (int channel = 0; channel < totalNumOutputChannels; ++channel)
    {
        // we define an additional angle of 0/90/180/270 for the channels 0-4
        // float additionalAngle = channel * 3.14 * 0.5;
        
        // we get a write pointer to the main buffer
        auto* channelData = buffer.getWritePointer (channel);

        // we loop through all samples in the buffer
        for(int iData = 0; iData < buffer.getNumSamples(); iData++)
        {
            // we define a factor that is dependent on the current time and produces a 1Hz sinus curve
            // float factor = sin((runningValue + iData) / getSampleRate() * 3.14 + additionalAngle);
            
            // now we use the ambisonics factor for the corresponding channel:
            float factor = ambiFactors[channel];
            
            // we multiply the input data with that factor and save it to the output buffer (through the write pointer)
            channelData[iData] = readPointer[iData] * factor;
        }
    }
    
    // finally we update our runningValue
    runningValue += buffer.getNumSamples();
}

//==============================================================================
bool DemoPluginAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* DemoPluginAudioProcessor::createEditor()
{
    return new DemoPluginAudioProcessorEditor (*this);
}

//==============================================================================
void DemoPluginAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void DemoPluginAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new DemoPluginAudioProcessor();
}
