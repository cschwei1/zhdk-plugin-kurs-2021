/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
DemoPluginAudioProcessorEditor::DemoPluginAudioProcessorEditor (DemoPluginAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p)
{
    // we create an instance of our class MainGui and save the pointer to the variable mainGui
    // since it's a uinque_ptr, all future memora management will be handled automatically by the unique_ptr itself
    mainGui.reset(new MainGui(&p.audioParams));
    
    // we tell JUCE to add the GUI element to the visible area
    addAndMakeVisible(mainGui.get());
    
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (400, 300);
}

DemoPluginAudioProcessorEditor::~DemoPluginAudioProcessorEditor()
{
    // because we manually created our main GUI, we have to delete it here!
}

//==============================================================================
void DemoPluginAudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));

    g.setColour (juce::Colours::white);
    g.setFont (15.0f);
    g.drawFittedText ("Hello World!", getLocalBounds(), juce::Justification::centred, 1);
}

void DemoPluginAudioProcessorEditor::resized()
{
    // we use the whole available gui space for our main GUI
    mainGui->setBounds(getBounds());
}
